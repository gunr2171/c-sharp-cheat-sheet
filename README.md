[[_TOC_]] 

# Types

## Primitive Types

* `int` - Integer (+/- 2 billion, whole number)
* `string` - Text
* `bool` - Boolean (`true` or `false`)

## Literals

* String literal - double quotes: `"abcd efg"`
* Integer literal - just the number: `123`
* Boolean literal - `true` or `false`

# Variables

## Declarations
```csharp
<type> <name>;
```

Examples:

```csharp
int myValue;
string myValue;
bool myValue;
```

## Assignment

```csharp
<variableName> = <something>;
```

Examples
```csharp
myMessage = "hello there"; // Assign from literal
myMessage = SomeMethod(3); // Assign from method result
myNumber = 4 + 3; // Assign from math operator result
isSnowing = isCold && isRaining; // Assign from boolean operator result

// or any combination
```

## Declaration + Assignment

Declare and assign a variable at the same time.
```csharp
<type> <name> = <something>;
```

For example:
```csharp
int myNumber = 5
bool isSnowing = isCold && isRaining;
```

## Shorthand variable type

When you don't want to write the variable type. Can only be used if you assign the variable in the same statement as you declare it.
```csharp
var <name> = <something>
```

```csharp
var isSnowing = isCold && isRaining;

// Variable is still a Boolean, you just let the compiler figure it out rather than explicitly type it.
```

# Console

```csharp
// Write text
System.Console.Write(string/int/bool);

// Write text + NewLine
System.Console.WriteLine(string/int/bool);

// Read user's input as a string
string userInput = System.Console.ReadLine(string/int/bool);
```

# Loops

* `while` loop - unknown number of loops, will test continuation before first cycle.
* `do while` loop - unknown number of loops, will test continuation after first cycle.
* `for` loop - known number of loops, will test continuation before each cycle.
* `foreach` loop - known number of loops, used to just get items in the collection.

## while loop

```csharp
while (myValue != "tom")
{
    // code
}
```

## do while loop
```csharp
do
{
    // code
} while (myValue < 33);
```

## for loop
```csharp
for (int i = 0; i <= 10; i++)
{
    // code, use 'i'
}
```

## foreach loop
```csharp
foreach(var item in list)
{
    // code, use 'item'
}
```

# Flow Control

## if statements

"else if" and "else" are optional

```csharp
if (expression)
{
    // code
}
else if (expression)
{
    // code
}
else
{
    // code
}
```

# Methods

```csharp
<returnType> <methodName>(<parameter1Type> <parameter1Name>, ...)
{

}
```

Examples:
```csharp
void SayHello(string personName)
{
    Console.WriteLine("Hello there " + personName + "! How are you?");
}

int AddNumbers(int num1, int num2)
{
    return num1 + num2;
}
```
